<?php
/**
 * @file watchdogalter.api.php
 */

/**
 * Alter or delete log entries from watchdog,
 *
 * @param array $log_entry
 */
function hook_watchdog_alter(&$log_entry) {
  // If others before us have unset this entry, nothing to do.
  if (!$log_entry) {
    return;
  }
  if (mymodule_log_is_minor($log_entry)) {
    $log_entry['severity'] =  WATCHDOG_NOTICE;
  }
  if (mymodule_suppress_watchdog()) {
    // Other modules expect an array.
    $log_entry = array();
  }
}